%=============================================================================%
% Copyright (c) 2023-2024 AlaskanEmily, Transnat Games.
% Licensed under BSD 4-clause (Trans Rights notice).
:- module tl2.rt.
%=============================================================================%
% TurboLisp builtin functions.
% These include some Mercury (*subr) and some Lisp (*expr) functions, as well
% as an assoc_list that defines all of these as props which can be used to
% construct defs for the runtime.
:- interface.
%=============================================================================%

:- use_module assoc_list.

%-----------------------------------------------------------------------------%

:- pred reverse `with_type` subr `with_inst` subr.
:- pred cons `with_type` subr `with_inst` subr.
:- pred car `with_type` subr `with_inst` subr.
:- pred cdr `with_type` subr `with_inst` subr.
:- pred caar `with_type` subr `with_inst` subr.
:- pred cadr `with_type` subr `with_inst` subr.
:- pred nth `with_type` subr `with_inst` subr.
:- pred nilp `with_type` subr `with_inst` subr.
:- pred not_nilp `with_type` subr `with_inst` subr.
:- pred when_ `with_type` subr_ext `with_inst` subr_ext.

:- pred gt `with_type` subr `with_inst` subr.
:- pred lt `with_type` subr `with_inst` subr.
:- pred ge `with_type` subr `with_inst` subr.
:- pred le `with_type` subr `with_inst` subr.
:- pred eq `with_type` subr `with_inst` subr.
:- pred ne `with_type` subr `with_inst` subr.

:- pred plus `with_type` subr `with_inst` subr.
:- pred sub `with_type` subr `with_inst` subr.
:- pred times `with_type` subr `with_inst` subr.
:- pred div_ `with_type` subr `with_inst` subr.
:- pred rem_ `with_type` subr `with_inst` subr.

:- pred append `with_type` subr `with_inst` subr.
:- pred reverse_prepend `with_type` subr `with_inst` subr.
:- pred string `with_type` subr `with_inst` subr.
:- pred stringp `with_type` subr `with_inst` subr.
:- pred not_stringp `with_type` subr `with_inst` subr.
:- pred atom `with_type` subr `with_inst` subr.
:- pred atomp `with_type` subr `with_inst` subr.
:- pred not_atomp `with_type` subr `with_inst` subr.

%-----------------------------------------------------------------------------%

:- func rt = assoc_list.assoc_list(string, props).

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.
:- import_module int.
:- import_module pair.
:- use_module solutions.
:- use_module string.

%-----------------------------------------------------------------------------%

:- func list_arg = s.
list_arg = atom(maybe.no, "lst").
:- func list_args = lst.
list_args = [list_arg|[]].

%-----------------------------------------------------------------------------%

:- func one_arg = s.
one_arg = atom(maybe.no, "x").
:- func one_args = lst.
one_args = [one_arg|[]].

%-----------------------------------------------------------------------------%

reverse(Info, Args, Out) :-
    ( if
        Args = [list(_, T)|[]]
    then
        Out = list(maybe.no, list.reverse(T))
    else
        string.append(line_prefix(Info), " Invalid reverse", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func reverse_props = props.
reverse_props = [subr(reverse, list_args)|[]].

%-----------------------------------------------------------------------------%

cons(Info, Args, Out) :-
    ( if
        Args = [H|[list(N2, T)|[]]]
    then
        Out = list(N2, [H|T])
    else
        string.append(line_prefix(Info), " Invalid cons", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func cons_args = lst.
cons_args = [atom(maybe.no, "car")|[atom(maybe.no, "cdr")|[]]].
:- func cons_props = props.
cons_props = [subr(cons, cons_args)|[]].

%-----------------------------------------------------------------------------%

car(_, Args, Out) :-
    ( if
        Args = [list(_, [Semi|_])|[]]
    then
        Out = Semi
    else
        Out = nil
    ).

:- func car_props = props.
car_props = [subr(car, list_args)|[]].

%-----------------------------------------------------------------------------%

cdr(_, Args, Out) :-
    ( if
        Args = [list(N, [_|Semi])|[]]
    then
        Out = list(N, Semi)
    else
        Out = nil
    ).

:- func cdr_props = props.
cdr_props = [subr(cdr, list_args)|[]].

%-----------------------------------------------------------------------------%

caar(_, Args, Out) :-
    ( if
        Args = [list(_, [list(_, [Semi|_])|_])|[]]
    then
        Out = Semi
    else
        Out = nil
    ).

:- func caar_props = props.
caar_props = [subr(caar, list_args)|[]].

%-----------------------------------------------------------------------------%

cadr(_, Args, Out) :-
    ( if
        Args = [list(_, [_|[Semi|_]])|[]]
    then
        Out = Semi
    else
        Out = nil
    ).

:- func cadr_props = props.
cadr_props = [subr(cadr, list_args)|[]].

%-----------------------------------------------------------------------------%

nth(_, Args, Out) :-
    ( if
        Args = [int(_, I)|[list(_, List)|[]]],
        list.index0(List, I, Semi)
    then
        Out = Semi
    else
        Out = nil
    ).

:- func nth_args = lst.
nth_args = [tl2.atom(maybe.no, "n")|list_args].
:- func nth_props = props.
nth_props = [subr(tl2.rt.nth, nth_args)|[]].

%-----------------------------------------------------------------------------%

nilp(_, Args, Out) :-
    ( Args = [list(_, [])|[]] -> Out = t ; Out = nil ).

:- func nilp_props = props.
nilp_props = [subr(nilp, list_args)|[]].

%-----------------------------------------------------------------------------%

not_nilp(_, Args, Out) :-
    ( Args = [list(_, [])|[]] -> Out = nil ; Out = t ).

:- func not_nilp_props = props.
not_nilp_props = [subr(not_nilp, list_args)|[]].

%-----------------------------------------------------------------------------%

when_(Info, Args, Env, Out, !Defs, !IO) :-
    ( if
        Args = [A|List]
    then
        tl2.eval(A, Env, Test, !Defs, !IO),
        ( if
            Test = tl2.list(_, [])
        then
            Out = Test
        else
            list.foldl3(tl2.evlis(Env), List, tl2.nil, Out, !Defs, !IO)
        )
    else
        string.append(line_prefix(Info), " Invalid when", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func when_args = lst.
when_args = [tl2.atom(maybe.no, "x")|list_args].
:- func when_props = props.
when_props = [fsubr_ext(when_, when_args)|[]].

%-----------------------------------------------------------------------------%

:- pred compare_element(s::in, s::in, builtin.comparison_result::out) is semidet.
compare_element(string(_, A), string(_, B), Cmp) :-
    builtin.compare(Cmp, A, B).
compare_element(int(_, A), int(_, B), Cmp) :-
    builtin.compare(Cmp, A, B).

%-----------------------------------------------------------------------------%

:- pred cmp(pred(builtin.comparison_result), info, lst, s).
:- mode cmp(pred(in) is semidet, in, in, out) is det.
cmp(Pred, Info, Args, Out) :-
    ( if
        Args = [A|[B|[]]],
        compare_element(A, B, Cmp)
    then
        ( Pred(Cmp) -> Out = t ; Out = nil )
    else
        string.append(line_prefix(Info), " Invalid compare", Error),
        exception.throw(exception.software_error(Error))
    ).
:- pragma no_inline(cmp/4).

%-----------------------------------------------------------------------------%

:- func pair_args = lst.
pair_args = [atom(maybe.no, "a")|[atom(maybe.no, "b")|[]]].

%-----------------------------------------------------------------------------%

gt(N, Args, Out) :-
    cmp((pred((>)::in) is semidet), N, Args, Out).

:- func gt_props = props.
gt_props = [subr(gt, pair_args)|[]].

%-----------------------------------------------------------------------------%

lt(N, Args, Out) :-
    cmp((pred((<)::in) is semidet), N, Args, Out).

:- func lt_props = props.
lt_props = [subr(lt, pair_args)|[]].

%-----------------------------------------------------------------------------%

ge(N, Args, Out) :-
    cmp((pred(C::in) is semidet :- C \= (<)), N, Args, Out).

:- func ge_props = props.
ge_props = [subr(ge, pair_args)|[]].

%-----------------------------------------------------------------------------%

le(N, Args, Out) :-
    cmp((pred(C::in) is semidet :- C \= (>)), N, Args, Out).

:- func le_props = props.
le_props = [subr(le, pair_args)|[]].

%-----------------------------------------------------------------------------%

eq(Info, Args, Out) :-
    ( if
        Args = [A|[B|[]]]
    then
        ( eqv(A, B) -> Out = t ; Out = nil )
    else
        string.append(line_prefix(Info), " Invalid =", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func eq_props = props.
eq_props = [subr(eq, pair_args)|[]].

%-----------------------------------------------------------------------------%

ne(Info, Args, Out) :-
    ( if
        Args = [A|[B|[]]]
    then
        ( not eqv(A, B) -> Out = t ; Out = nil )
    else
        string.append(line_prefix(Info), " Invalid !=", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func ne_props = props.
ne_props = [subr(ne, pair_args)|[]].

%-----------------------------------------------------------------------------%

:- pred arith(func(int, int) = int, info, lst, s).
:- mode arith(func(in, in) = (out) is det, in, in, out) is det.
arith(Fn, Info, Args, Out) :-
    ( if
        Args = [int(_, A)|[int(_, B)|[]]]
    then
        Out = int(maybe.no, Fn(A, B))
    else
        string.append(line_prefix(Info), " Invalid arithmetic", Error),
        exception.throw(exception.software_error(Error))
    ).

%-----------------------------------------------------------------------------%

plus(N, Args, Out) :-
    arith(int.plus, N, Args, Out).

:- func plus_props = props.
plus_props = [subr(plus, pair_args)|[]].

%-----------------------------------------------------------------------------%

sub(N, Args, Out) :-
    arith(int.minus, N, Args, Out).

:- func sub_props = props.
sub_props = [subr(sub, pair_args)|[]].

%-----------------------------------------------------------------------------%

times(N, Args, Out) :-
    arith(int.times, N, Args, Out).

:- func times_props = props.
times_props = [subr(times, pair_args)|[]].

%-----------------------------------------------------------------------------%

div_(N, Args, Out) :-
    arith((func(A, B) = int.div(A, B)), N, Args, Out).

:- func div_props = props.
div_props = [subr(div_, pair_args)|[]].

%-----------------------------------------------------------------------------%

rem_(N, Args, Out) :-
    arith((func(A, B) = (A mod B)), N, Args, Out).

:- func rem_props = props.
rem_props = [subr(rem_, pair_args)|[]].

%-----------------------------------------------------------------------------%

append(Info, Args, Out) :-
    ( if
        Args = [tl2.string(_, A)|[tl2.string(_, B)|[]]]
    then
        Out = tl2.string(Info, string.append(A, B))
    else if
        Args = [tl2.list(_, A)|[tl2.list(_, B)|[]]]
    then
        Out = tl2.list(Info, list.append(A, B))
    else
        string.append(line_prefix(Info), " Invalid append", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func append_props = props.
append_props = [subr(tl2.rt.append, pair_args)|[]].

%-----------------------------------------------------------------------------%

reverse_prepend(Info, Args, Out) :-
    ( if
        Args = [tl2.list(_, A)|[tl2.list(_, B)|[]]]
    then
        Out = tl2.list(Info, list.reverse_prepend(A, B))
    else
        string.append(line_prefix(Info), " Invalid reverse-prepend", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func reverse_prepend_props = props.
reverse_prepend_props = [subr(tl2.rt.reverse_prepend, pair_args)|[]].

%-----------------------------------------------------------------------------%

string(Info, Args, Out) :-
    ( if
        Args = [S @ tl2.string(_, _)|_]
    then
        Out = S
    else if
        Args = [Head|_],
        (
            Head = tl2.atom(_, Str)
        ;
            Head = tl2.int(_, I), Str = string.from_int(I)
        )
    then
        Out = tl2.string(Info, Str)
    else
        string.append(line_prefix(Info), " Invalid string coercion", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func string_props = props.
string_props = [subr(tl2.rt.string, one_args)|[]].

%-----------------------------------------------------------------------------%

stringp(_Info, Args, Out) :-
    ( Args = [tl2.string(_, _)|_] -> Out = t ; Out = nil ).

:- func stringp_props = props.
stringp_props = [subr(stringp, one_args)|[]].

%-----------------------------------------------------------------------------%

not_stringp(_Info, Args, Out) :-
    ( Args = [tl2.string(_, _)|_] -> Out = nil ; Out = t ).

:- func not_stringp_props = props.
not_stringp_props = [subr(not_stringp, one_args)|[]].

%-----------------------------------------------------------------------------%

atom(Info, Args, Out) :-
    ( if
        Args = [tl2.string(_, Str)|_]
    then
        Out = tl2.atom(Info, Str)
    else if
        Args = [A @ tl2.atom(_, _)|_]
    then
        Out = A
    else
        string.append(line_prefix(Info), " Invalid atom coercion", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func atom_props = props.
atom_props = [subr(tl2.rt.atom, one_args)|[]].

%-----------------------------------------------------------------------------%

atomp(_Info, Args, Out) :-
    ( Args = [tl2.atom(_, _)|_] -> Out = t ; Out = nil ).

:- func atomp_props = props.
atomp_props = [subr(atomp, one_args)|[]].

%-----------------------------------------------------------------------------%

not_atomp(_Info, Args, Out) :-
    ( Args = [tl2.atom(_, _)|_] -> Out = nil ; Out = t ).

:- func not_atomp_props = props.
not_atomp_props = [subr(not_atomp, one_args)|[]].

%-----------------------------------------------------------------------------%

:- func nil_props = props.
nil_props = [apval(list(maybe.no, []))|[]].

%-----------------------------------------------------------------------------%

:- func t_props = props.
t_props = [apval(t)|[]].

%-----------------------------------------------------------------------------%

:- pred eval_ `with_type` subr_ext `with_inst` subr_ext.
eval_(Info, Args, Env, Out, !Defs, !IO) :-
    ( if
        Args = [Val|[]]
    then
        tl2.eval(Val, Env, Out, !Defs, !IO)
    else
        string.append(line_prefix(Info), " Invalid eval", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func eval_props = props.
eval_props = [subr_ext(eval_, [atom(maybe.no, "e")|[]])|[]].

%-----------------------------------------------------------------------------%

:- func gensym(int) = string.

gensym(Sym) = Atom :-
    string.pad_left(string.from_int(Sym), ('0'), 6, N),
    string.first_char(Atom, ('g'), N).

:- pragma foreign_decl("C", "
#define TL2_NUM_GENSYM_STUFF 8
extern const char TL2_gensym_stuff[TL2_NUM_GENSYM_STUFF][8];
    ").
:- pragma foreign_code("C", "
/* For shorter scripts, don't actually allocate these. */
const char TL2_gensym_stuff[TL2_NUM_GENSYM_STUFF][8] = {
    ""g000001"",
    ""g000002"",
    ""g000003"",
    ""g000004"",
    ""g000005"",
    ""g000006"",
    ""g000007"",
    ""g000008""
};
    ").

:- pragma foreign_proc("C",
    gensym(Sym::in) = (Atom::out),
    [promise_pure, thread_safe, does_not_affect_liveness, may_duplicate,
    will_not_call_mercury, will_not_throw_exception],
    "
    if(Sym > 0 && Sym < TL2_NUM_GENSYM_STUFF + 1){
        Atom = (MR_String)(TL2_gensym_stuff[Sym - 1]);
    }
    else{
        MR_allocate_aligned_string_msg(Atom, 7, MR_ALLOC_ID);
        MR_Integer i;
        i = 7;
        Atom[i--] = 0;
        while(i != 0){
            if(Sym == 0){
                Atom[i--] = '0';
            }
            else{
                Atom[i--] = '0' + (Sym % 10);
                Sym /= 10;
            }
        }
        Atom[0] = 'g';
    }
    ").

%-----------------------------------------------------------------------------%

:- mutable(gensym, int, 0, ground, [attach_to_io_state, untrailed]).
:- pred gensym `with_type` subr_ext `with_inst` subr_ext.
gensym(Info, _Args, _Env, atom(Info, gensym(Sym)), !Defs, !IO) :-
    get_gensym(Sym - 1, !IO),
    set_gensym(Sym, !IO).

:- func gensym_props = props.
gensym_props = [subr_ext(gensym, [])|[]].

%-----------------------------------------------------------------------------%

:- pred sassoc `with_type` subr `with_inst` subr.
tl2.rt.sassoc(Info, Args, Out) :-
    ( if
        Args = [Key|[tl2.list(_, List)|[]]]
    then
        ( tl2.sassoc(Key, List, Semi) -> Out = Semi ; Out = nil )
    else
        string.append(line_prefix(Info), " Invalid sassoc", Error),
        exception.throw(exception.software_error(Error))
    ).

:- func sassoc_args = (lst::out(tl2.one_or_more)) is det.
sassoc_args = [tl2.atom(maybe.no, "k")|[tl2.atom(maybe.no, "lst")|[]]].
:- func sassoc_props = props.
sassoc_props = [subr(tl2.rt.sassoc, sassoc_args)|[]].

%-----------------------------------------------------------------------------%

rt = [
    ("nil" - nil_props),
    ("t" - t_props),
    ("eval" - eval_props),
    ("gensym" - gensym_props),
    ("reverse" - reverse_props),
    ("cons" - cons_props),
    ("car" - car_props),
    ("cdr" - cdr_props),
    ("caar" - caar_props),
    ("cadr" - cadr_props),
    ("nth" - nth_props),
    ("gt" - gt_props),
    (">" - gt_props),
    ("lt" - lt_props),
    ("<" - lt_props),
    ("ge" - ge_props),
    (">=" - ge_props),
    ("le" - le_props),
    ("<=" - le_props),
    ("eq" - eq_props),
    ("=" - eq_props),
    ("ne" - ne_props),
    ("!=" - ne_props),
    ("+" - plus_props),
    ("add" - plus_props),
    ("-" - sub_props),
    ("sub" - sub_props),
    ("*" - times_props),
    ("times" - times_props),
    ("/" - div_props),
    ("div" - div_props),
    ("%" - rem_props),
    ("rem" - rem_props),
    ("append" - append_props),
    ("reverse-prepend" - reverse_prepend_props),
    % ("sassoc" - sassoc_props),
    ("string" - string_props),
    ("string?" - stringp_props),
    ("not-string?" - stringp_props),
    ("atom" - atom_props),
    ("atom?" - atomp_props),
    ("not-atom?" - not_atomp_props),
    ("nil?" - nilp_props),
    ("not-nil?" - not_nilp_props),
    ("when" - when_props)].

