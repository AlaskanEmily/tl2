%=============================================================================%
% Copyright (c) 2023-2024 AlaskanEmily, Transnat Games.
% Licensed under BSD 4-clause (Trans Rights notice).
:- module tl2.parser.
%=============================================================================%
% TurboLisp parser.
:- interface.
%=============================================================================%

:- use_module stream.

%-----------------------------------------------------------------------------%

:- type error(Error) --->
    stream_error(Error) ;
    unexpected(filename, lineno, string) ;
    unmatched(filename, lineno, string).

%-----------------------------------------------------------------------------%

:- type error --->
    unexpected(filename, lineno, string) ;
    unmatched(filename, lineno, string).

%-----------------------------------------------------------------------------%

:- type result --->
    ok(s) ;
    eof ;
    error(error).

%-----------------------------------------------------------------------------%

:- pred convert_error(error(_), error).
:- mode convert_error(in, out) is semidet.
:- mode convert_error(di, uo) is semidet.
:- mode convert_error(out, in) is det.
:- mode convert_error(uo, di) is det.
:- mode convert_error(in, in) is semidet. % Implied.

%-----------------------------------------------------------------------------%

:- pred parse(Stream, filename, stream.result(s, error(Error)), lineno, lineno, State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse(in, in, out, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- func parse(filename, string) = result.

%-----------------------------------------------------------------------------%
% parse(FileName, Src, SrcLen, Out, !SrcStart).
% Begin parsing at a certain offset.
:- pred parse(filename, string, int, result, int, int).
:- mode parse(in, in, in, out, in, out) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module bool.
:- use_module char.
:- import_module int.
:- use_module string.

%-----------------------------------------------------------------------------%

convert_error(unexpected(F, I, S), unexpected(F, I, S)).
convert_error(unmatched(F, I, S), unmatched(F, I, S)).
:- pragma inline(convert_error/2).

%-----------------------------------------------------------------------------%

:- pred is_ws(character::in) is semidet.
is_ws(' ').
is_ws('\r').
is_ws('\t').

%-----------------------------------------------------------------------------%

:- pred is_atom(character::in) is semidet.
is_atom('-').
is_atom('/').
is_atom('%').
is_atom('&').
is_atom('|').
is_atom('@').
is_atom('!').
is_atom('#').
is_atom('$').
is_atom('^').
is_atom('*').
is_atom('+').
is_atom('=').
is_atom('<').
is_atom('>').
is_atom('?').
is_atom('~').
is_atom(C) :- char.is_alnum_or_underscore(C).

%-----------------------------------------------------------------------------%

:- type class --->
    atom(character) ;
    quote ;
    squote ;
    list ;
    endlist ;
    comment ;
    dash ;
    ws(int) ;
    int(int) ;
    error(character).

%-----------------------------------------------------------------------------%

:- pred break_class(class::in) is semidet.
break_class(endlist).
break_class(comment).
break_class(ws(_)).
break_class(error(_)).

%-----------------------------------------------------------------------------%

:- pred classify_direct(character::in, class::out) is semidet.
classify_direct(('"'), quote).
classify_direct(('\''), squote).
classify_direct(('('), list).
classify_direct((')'), endlist).
classify_direct((';'), comment).
classify_direct(('-'), dash).
classify_direct(('\n'), ws(1)).
classify_direct((' '), ws(0)).
classify_direct(('\r'), ws(0)).
classify_direct(('\t'), ws(0)).
classify_direct(('0'), int(0)).
classify_direct(('1'), int(1)).
classify_direct(('2'), int(2)).
classify_direct(('3'), int(3)).
classify_direct(('4'), int(4)).
classify_direct(('5'), int(5)).
classify_direct(('6'), int(6)).
classify_direct(('7'), int(7)).
classify_direct(('8'), int(8)).
classify_direct(('9'), int(9)).

:- func classify(character) = class.
classify(Char) = Class :-
    ( if
        classify_direct(Char, Semi)
    then
        Class = Semi
    else if
        is_atom(Char)
    then
        Class = atom(Char)
    else
        Class = error(Char)
    ).

%-----------------------------------------------------------------------------%

:- pred parse_list(
    Stream,
    filename,
    lineno,
    lst,
    stream.result(s, error(Error)),
    lineno, lineno,
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_list(in, in, in, in, out, in, out, di, uo) is det.
parse_list(Stream, Filename, Start, List, Result, !Line, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Result = stream.error(unmatched(Filename, Start, "("))
    ;
        CharResult = stream.ok,
        classify(Char) = Class,
        % TODO: Factor this out as a "skip to something interesting" pred.
        % That shouldn't include 'endlist'!
        ( if
            Class = endlist
        then
            Info = maybe.yes(line_data(Filename, Start)),
            Result = stream.ok(list(Info, list.reverse(List)))
        else if
            Class = ws(I)
        then
            !:Line = !.Line + I,
            parse_list(Stream, Filename, Start, List, Result, !Line, !State)
        else if
            Class = comment
        then
            skip_comment(Stream, CommentResult, !State),
            (
                CommentResult = stream.eof,
                Result = stream.eof
            ;
                CommentResult = stream.error(Error),
                Result = stream.error(stream_error(Error))
            ;
                CommentResult = stream.ok,
                !:Line = !.Line + 1,
                parse_list(Stream, Filename, Start, List, Result, !Line, !State)
            )
        else if
            Class = error(C)
        then
            Error = unexpected(Filename, !.Line, string.duplicate_char(C, 1)),
            Result = stream.error(Error)
        else
            parse_value(Stream, Filename, Class, ValueResult, !Line, !State),
            (
                ValueResult = stream.eof,
                Result = stream.error(unmatched(Filename, Start, "("))
            ;
                ValueResult = stream.error(Error),
                Result = stream.error(Error)
            ;
                ValueResult = stream.ok(Value),
                parse_list(Stream, Filename, Start, [Value|List], Result, !Line, !State)
            )
        )
    ).

%-----------------------------------------------------------------------------%

:- pred parse_escape(
    Stream,
    filename,
    lineno,
    stream.res(error(Error)),
    list.list(character), list.list(character),
    lineno, lineno,
    State, State)
    <= stream.unboxed_reader(Stream, character, State, Error).
:- mode parse_escape(in, in, in, out, in, out, in, out, di, uo) is det.

parse_escape(Stream, Filename, Start, Res, !Chars, !Line, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Res = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Res = stream.error(unmatched(Filename, Start, """"))
    ;
        CharResult = stream.ok,
        ( if
            (
                Char = ('n'), X = ('\n')
            ;
                Char = ('r'), X = ('\r')
            ;
                Char = ('"'), X = ('"')
            ;
                Char = ('\\'), X = ('\\')
            )
        then
            list.cons(X, !Chars),
            Res = stream.ok
        else if
            Char = ('\n')
        then
            !:Line = !.Line + 1,
            Res = stream.ok
        else if
            is_ws(Char)
        then
            Res = stream.ok
        else
            string.from_char_list([('\\')|[Char|[]]]) = Error,
            Res = stream.error(unexpected(Filename, !.Line, Error))
        )
    ).

%-----------------------------------------------------------------------------%

:- pred parse_string(
    Stream,
    filename,
    list.list(character),
    lineno,
    stream.result(s, error(Error)),
    lineno, lineno,
    State, State)
    <= stream.unboxed_reader(Stream, character, State, Error).
:- mode parse_string(in, in, in, in, out, in, out, di, uo) is det.
parse_string(Stream, Filename, Chars, Start, Result, !Line, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Info = maybe.yes(line_data(Filename, Start)),
        Result = stream.ok(string(Info, string.from_rev_char_list(Chars)))
    ;
        CharResult = stream.ok,
        ( if
            Char = ('\\')
        then
            parse_escape(Stream, Filename, Start, Res, Chars, NewChars, !Line, !State),
            (
                Res = stream.error(Error),
                Result = stream.error(Error)
            ;
                Res = stream.ok,
                parse_string(Stream, Filename, NewChars, Start, Result, !Line, !State)
            )
        else if
            Char = ('"')
        then
            Info = maybe.yes(line_data(Filename, Start)),
            Result = stream.ok(string(Info, string.from_rev_char_list(Chars)))
        else
            ( Char = ('\n') -> !:Line = !.Line + 1 ; true ),
            NewChars = [Char|Chars],
            parse_string(Stream, Filename, NewChars, Start, Result, !Line, !State)
        )
    ).

%-----------------------------------------------------------------------------%

:- pred parse_atom(
    Stream,
    filename,
    list.list(character),
    lineno,
    stream.result(s, error(Error)),
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_atom(in, in, in, in, out, di, uo) is det.
parse_atom(Stream, Filename, Chars, Line, Result, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Info = maybe.yes(line_data(Filename, Line)),
        Result = stream.ok(atom(Info, string.from_rev_char_list(Chars)))
    ;
        CharResult = stream.ok,
        ( if
            is_ws(Char)
        then
            Info = maybe.yes(line_data(Filename, Line)),
            Result = stream.ok(atom(Info, string.from_rev_char_list(Chars)))
        else if
            is_atom(Char)
        then
            parse_atom(Stream, Filename, [Char|Chars], Line, Result, !State)
        else
            stream.unget(Stream, Char, !State),
            Info = maybe.yes(line_data(Filename, Line)),
            Result = stream.ok(atom(Info, string.from_rev_char_list(Chars)))
        )
    ).

%-----------------------------------------------------------------------------%

:- pred parse_int(
    Stream,
    filename,
    int,
    lineno,
    stream.res(int, error(Error)),
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_int(in, in, in, in, out, di, uo) is det.
parse_int(Stream, Filename, I, Line, Res, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Res = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Res = stream.ok(I)
    ;
        CharResult = stream.ok,
        ( if
            char.decimal_digit_to_int(Char, D)
        then
            parse_int(Stream, Filename, (I * 10) + D, Line, Res, !State)
        else if
            is_ws(Char) ;
            Char = (';') ; Char = ('\n') ; Char = ('(') ; Char = (')')
        then
            stream.unget(Stream, Char, !State),
            Res = stream.ok(I)
        else
            Error = unexpected(Filename, Line, string.duplicate_char(Char, 1)),
            Res = stream.error(Error)
        )
    ).

%-----------------------------------------------------------------------------%

:- pred parse_int(
    Stream,
    filename,
    int,
    bool.bool,
    lineno,
    stream.result(s, error(Error)),
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_int(in, in, in, in, in, out, di, uo) is det.

parse_int(Stream, Filename, !.I, Negate, Line, Result, !State) :-
    parse_int(Stream, Filename, !.I, Line, Res, !State),
    (
        Res = stream.ok(!:I),
        (
            Negate = bool.no
        ;
            Negate = bool.yes,
            !:I = -(!.I)
        ),
        Result = stream.ok(int(maybe.yes(line_data(Filename, Line)), !.I))
    ;
        Res = stream.error(Error),
        Result = stream.error(Error)
    ).

%-----------------------------------------------------------------------------%

:- pred parse_squote(
    Stream,
    filename,
    stream.result(s, error(Error)),
    int, int,
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_squote(in, in, out, in, out, di, uo) is det.

parse_squote(Stream, Filename, Result, !Line, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    Start = !.Line,
    EOF = stream.error(unexpected(Filename, Start, "EOF")),
    (
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Result = EOF
    ;
        CharResult = stream.ok,
        classify(Char) = Class,
        ( if
            break_class(Class)
        then
            Error = unexpected(Filename, Start, string.duplicate_char(Char, 1)),
            Result = stream.error(Error)
        else
            Quote = atom(maybe.yes(line_data(Filename, Start)), "quote"),
            parse_value(Stream, Filename, Class, ValueResult, !Line, !State),
            (
                ValueResult = stream.error(Error),
                Result = stream.error(Error)
            ;
                ValueResult = stream.eof,
                Result = EOF
            ;
                ValueResult = stream.ok(Value),
                Info = maybe.yes(line_data(Filename, Start)),
                Result = stream.ok(list(Info, [Quote|[Value|[]]]))
            )
        )
    ).
:- pragma inline(parse_squote/7).

%-----------------------------------------------------------------------------%

:- pred parse_dash(
    Stream,
    filename,
    lineno,
    stream.result(s, error(Error)),
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_dash(in, in, in, out, di, uo) is det.
parse_dash(Stream, Filename, Line, Result, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.eof,
        Result = stream.ok(atom(maybe.yes(line_data(Filename, Line)), "-"))
    ;
        CharResult = stream.ok,
        ( if
            (
                Char = ('"'), ErrorStr = """"
            ;
                Char = ('('), ErrorStr = "("
            )
        then
            Result = stream.error(unexpected(Filename, Line, ErrorStr))
        else if
            char.decimal_digit_to_int(Char, I)
        then
            parse_int(Stream, Filename, I, bool.yes, Line, Result, !State)
        else if
            is_atom(Char)
        then
            Chars = [Char|[('-')|[]]],
            parse_atom(Stream, Filename, Chars, Line, Result, !State)
        else
            stream.unget(Stream, Char, !State),
            Result = stream.ok(atom(maybe.yes(line_data(Filename, Line)), "-"))
        )
    ).
:- pragma inline(parse_dash/6).

%-----------------------------------------------------------------------------%

:- pred skip_comment(Stream, stream.result(Error), State, State)
    <= stream.unboxed_reader(Stream, character, State, Error).
:- mode skip_comment(in, out, di, uo) is det.
skip_comment(Stream, Res, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.eof,
        Res = stream.eof
    ;
        CharResult = stream.error(Error),
        Res = stream.error(Error)
    ;
        CharResult = stream.ok,
        ( if
            Char = ('\n')
        then
            Res = stream.ok
        else
            skip_comment(Stream, Res, !State)
        )
    ).
:- pragma inline(skip_comment/4).

%-----------------------------------------------------------------------------%

:- pred parse_value(
    Stream,
    filename,
    class,
    stream.result(s, error(Error)),
    lineno, lineno,
    State, State)
    <= (stream.unboxed_reader(Stream, character, State, Error),
        stream.putback(Stream, character, State, Error)).
:- mode parse_value(in, in, in, out, in, out, di, uo) is det.

parse_value(Stream, Filename, atom(C), Result, !Line, !State) :-
    parse_atom(Stream, Filename, [C|[]], !.Line, Result, !State).

parse_value(Stream, Filename, quote, Result, !Line, !State) :-
    parse_string(Stream, Filename, [], !.Line, Result, !Line, !State).

parse_value(Stream, Filename, squote, Result, !Line, !State) :-
    parse_squote(Stream, Filename, Result, !Line, !State).

parse_value(Stream, Filename, list, Result, !Line, !State) :-
    parse_list(Stream, Filename, !.Line, [], Result, !Line, !State).

parse_value(_Stream, Filename, endlist, stream.error(Error), !Line, !State) :-
    Error = unmatched(Filename, !.Line, ")").

parse_value(Stream, Filename, comment, Result, !Line, !State) :-
    skip_comment(Stream, CommentResult, !State),
    (
        CommentResult = stream.eof,
        Result = stream.eof
    ;
        CommentResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CommentResult = stream.ok,
        parse(Stream, Filename, Result, !.Line + 1, !:Line, !State)
    ).

parse_value(Stream, Filename, dash, Result, !Line, !State) :-
    parse_dash(Stream, Filename, !.Line, Result, !State).

parse_value(Stream, Filename, ws(I), Result, !Line, !State) :-
    parse(Stream, Filename, Result, !.Line + I, !:Line, !State).

parse_value(Stream, Filename, int(I), Result, !Line, !State) :-
    parse_int(Stream, Filename, I, bool.no, !.Line, Result, !State).

parse_value(_Stream, Filename, error(C), stream.error(Error), !Line, !State) :-
    Error = unexpected(Filename, !.Line, string.duplicate_char(C, 1)).

%-----------------------------------------------------------------------------%

parse(Stream, Filename, Result, !Line, !State) :-
    stream.unboxed_get(Stream, CharResult, Char, !State),
    (
        CharResult = stream.eof,
        Result = stream.eof
    ;
        CharResult = stream.error(Error),
        Result = stream.error(stream_error(Error))
    ;
        CharResult = stream.ok,
        parse_value(Stream, Filename, classify(Char), Result, !Line, !State)
    ).

%-----------------------------------------------------------------------------%

:- type string_stream ---> string_stream(string, int).

:- type string_stream_state ---> s(int, list.list(character)).

:- type utf_error ---> utf_error.

:- instance stream.error(utf_error) where [
    error_message(utf_error) = "Invalid UTF-8"
].

:- instance stream.stream(string_stream, string_stream_state) where [
    name(_, "tl2_string_stream", !S)
].

:- instance stream.input(string_stream, string_stream_state) where [].

:- instance stream.reader(string_stream, character, string_stream_state, utf_error) where [
    (get(Stream, Result, !State) :-
        stream.unboxed_get(Stream, CharResult, Char, !State),
        (
            CharResult = stream.ok,
            Result = stream.ok(Char)
        ;
            CharResult = stream.eof,
            Result = stream.eof
        ;
            CharResult = stream.error(Error),
            Result = stream.error(Error)
        )
    )
].

:- instance stream.unboxed_reader(string_stream, character, string_stream_state, utf_error) where [
    (unboxed_get(string_stream(Str, End), Result, Out, s(!.I, !.List), s(!:I+0, !:List)) :-
        (
            !.List = [],
            ( if
                string.index_next(Str, !I, C),
                !.I =< End
            then
                Out = C,
                Result = stream.ok
            else
                Out = (' '),
                Result = stream.eof
            )
        ;
            !.List = [Out|!:List],
            Result = stream.ok
        )
    )
].

:- instance stream.putback(string_stream, character, string_stream_state, utf_error) where [
    (unget(string_stream(Str, _End), C, s(!.I, !.L), s(!:I+0, !:L)) :-
        % If we can, just rewind the index.
        ( if
            % We already must have nothing pushed.
            !.L = [],
            % The previous character must be identical.
            string.prev_index(Str, !I, C)
        then
            true
        else
            !:L = [builtin.unsafe_promise_unique(C)|!.L]
        )
    )
].

:- instance stream.unbounded_putback(string_stream, character, string_stream_state, utf_error) where [].

%-----------------------------------------------------------------------------%

parse(Filename, Src, End, Out, !I) :-
    Stream = string_stream(Src, End),
    parse(Stream, Filename, Result, 0, _, s(!.I+0, []), s(!:I, O)),
    !:I = !.I - list.length(O),
    (
        Result = stream.ok(Value),
        Out = ok(Value)
    ;
        Result = stream.eof,
        Out = eof
    ;
        Result = stream.error(stream_error(_)),
        % UHHHHH...
        Out = eof
    ;
        Result = stream.error(unexpected(Filename2, Line, Error)),
        Out = error(unexpected(Filename2, Line, Error))
    ;
        Result = stream.error(unmatched(Filename2, Line, Error)),
        Out = error(unmatched(Filename2, Line, Error))
    ).

%-----------------------------------------------------------------------------%

parse(Filename, Src) = Out :-
    parse(Filename, Src, string.length(Src), Out, 0, _).

