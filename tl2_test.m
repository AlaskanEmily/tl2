%=============================================================================%
% Copyright (c) 2023-2024 AlaskanEmily, Transnat Games.
% Licensed under BSD 4-clause (Trans Rights notice).
:- module tl2_test.
%=============================================================================%
% TurboLisp tester.
:- interface.
%=============================================================================%

:- use_module io.

%-----------------------------------------------------------------------------%

:- pred main(io.io::di, io.io::uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module bool.
:- use_module exception.
:- use_module kv_list.
:- import_module list.
:- use_module rbtree.
:- use_module stream.
:- use_module string.

:- import_module tl2.
:- import_module tl2.parser.
:- import_module tl2.rt.

%-----------------------------------------------------------------------------%

:- pred read_until_blank(io.text_input_stream::in, bool.bool::out, io.io::di, io.io::uo) is det.
read_until_blank(Stream, Continue, !IO) :-
    io.read_line_as_string(Stream, LineResult, !IO),
    (
        LineResult = io.eof, Continue = bool.no
    ;
        LineResult = io.error(Error),
        io.write_line(Error, !IO),
        Continue = bool.no
    ;
        LineResult = io.ok(S),
        E = string.strip(S),
        ( if
            E = "continue" ; E = "'continue'"
        then
            io.write_string("; OK\n", !IO),
            Continue = bool.yes
        else
            read_until_blank(Stream, Continue, !IO)
        )
    ).

%-----------------------------------------------------------------------------%

:- type read_mode --->
    dump ;
    raw_dump ;
    normal.

%-----------------------------------------------------------------------------%

:- pred read(io.text_input_stream, read_mode, int, int, io.io, io.io).
:- mode read(in, in, in, out, di, uo) is det.
read(Stream, Mode, !Line, !IO) :-
    io.input_stream_name(Stream, Filename, !IO),
    parse(Stream, Filename, ValueResult, !Line, !IO),
    (
        ValueResult = stream.eof
    ;
        ValueResult = stream.ok(Value),
        (
            Mode = dump,
            io.write_string("=> ", !IO),
            %io.write_string(tl2.to_string(rbtree.init, Value), !IO),
            %io.nl(!IO)
            io.write_line(Value, !IO)
        ;
            Mode = raw_dump,
            io.write_string("=> ", !IO),
            io.write_line(Value, !IO)
        ;
            Mode = normal,
            RT = rbtree.from_assoc_list(rt),
            promise_equivalent_solutions [EvalResult, !:IO] (
                exception.try_io(
                    (pred(X::out, !.XIO::di, !:XIO::uo) is det :-
                        eval(Value, kv_list.kv_nil, X, RT, _, !XIO)
                    ),
                    EvalResult,
                    !IO)
            ),
            (
                EvalResult = exception.succeeded(Result),
                io.write_string("=> ", !IO),
                %io.write_string(tl2.pretty_print(RT, Result), !IO),
                %io.nl(!IO)
                io.write_line(Result, !IO)
            ;
                EvalResult = exception.exception(Exception),
                io.write_string("! ", !IO),
                io.write_line(Exception, !IO)
            )
        ),
        read(Stream, Mode, !Line, !IO)
    ;
        ValueResult = stream.error(stream_error(Error)),
        io.write_line(Error, !IO)
    ;
        (
            ValueResult = stream.error(unexpected(_, I, What)),
            Error = string.format("; Unexpected %s at line %i\n",
                [string.s(What)|[string.i(I)|[]]])
        ;
            ValueResult = stream.error(unmatched(_, I, What)),
            Error = string.format("; Unmatched %s at line %i\n",
                [string.s(What)|[string.i(I)|[]]])
        ),
        io.write_string(Error, !IO),
        io.write_string("\n; Type 'continue' to resume.\n", !IO),
        read_until_blank(Stream, Continue, !IO),
        (
            Continue = bool.no
        ;
            Continue = bool.yes,
            read(Stream, Mode, 0, _, !IO)
        )
    ).

%-----------------------------------------------------------------------------%

main(!IO) :-
    io.stdin_stream(Input, !IO),
    read(Input, normal, 1, _, !IO).

