; Any copyright is dedicated to the Public Domain.
; https://creativecommons.org/publicdomain/zero/1.0/

; Default runtime library for TurboLisp 2.
; This expects the default RT from tl2.rt to have been instantiated.

; Reduces arguments over the list.
(def reduce
  (fn (lst f d)
    (cond
      ((nil? lst) d)
      (t (reduce (cdr lst) f (f (car lst) d))))))

; Partially maps a list.
; You probably want map instead, this is used to implement that.
(def map-prepend
  (fn (lst f in)
    (cond
      ((nil? lst) (reverse in))
      (t (map-prepend (cdr lst) f (cons (f (car lst)) in))))))
    
; Maps a list
(def map (fn (lst f) (map-prepend lst f nil)))

; Yields the N'th item of a list, or nil if n is out of range.
(def nth-
  (fn (n lst)
    (cond
      ((nil? lst) nil)
      ((< n 1) (car lst))
      (t (nth (- n 1) (cdr lst))))))

; Finds the pair of a list that with a first element equivalent to v.
; Returns the pair, or nil if no element is found.
; This also distinguishes a list of *just* v, versus not being found (as would
; be ambiguous if it returned the cdr of the pair).
(def sassoc
  (fn (v lst)
    (cond
      ((nil? lst) nil)
      ((= (caar lst) v) (car lst))
      (t (sassoc v (cdr lst))))))

(def mod-assoc-prepend
  (fn (prefix lst v f)
    (cond
      ((nil? lst) (reverse prefix))
      ((= (caar lst) v)
        (reverse-prepend (cons (f (car lst)) prefix) (cdr lst)))
      (t (mod-assoc-prepend (cons (car lst) prefix) (cdr lst) v f)))))

; Finds a matching assoc, runs the provided operation 'f' on it, and then
; inserts it back in the list.
(def mod-assoc
  (fn (lst v f)
    (cond
      ((nil? lst) nil)
      (t (mod-assoc-prepend nil lst v f)))))

; Less common c*r functions that aren't builtins.
(def cddr '(lambda (l) (cdr (cdr l))))
(def cdar '(lambda (l) (cdr (car l))))
(def cdddr '(lambda (l) (cdr (cdr (cdr l)))))
(def caddr '(lambda (l) (car (cdr (cdr l)))))
(def cdadr '(lambda (l) (cdr (car (cdr l)))))
(def caadr '(lambda (l) (car (car (cdr l)))))
(def cddar '(lambda (l) (cdr (cdr (car l)))))
(def cadar '(lambda (l) (car (cdr (car l)))))
(def cdaar '(lambda (l) (cdr (car (car l)))))
(def caaar '(lambda (l) (car (car (car l)))))

(def cadddr '(lambda (l) (car (cdr (cdr (cdr l))))))
(def caddddr '(lambda (l) (car (cdr (cdr (cdr (cdr l)))))))


