# Any copyright is dedicated to the Public Domain.
# https://creativecommons.org/publicdomain/zero/1.0/

MERCURY?=mmc
GRADE_FOUND!='$(MERCURY)' --output-grade-string
GRADE_FOUND?=hlc.gc
GRADE?=$(GRADE_FOUND)
PARALLEL?=4
MMC_OPTS?=--mercury-linkage static -O 7 --intermodule-optimization --output-compile-error-lines 1024
MMC_OPTS+=--use-grade-subdirs -j $(PARALLEL) --grade $(GRADE)

all:
	'$(MERCURY)' $(MMC_OPTS) --make libtl2

repl: all
	'$(MERCURY)' $(MMC_OPTS) --make tl2_test

clean:
	rm -f libtl2.a libtl2.so libtl2.dylib tl2.init
	'$(MERCURY)' --grade $(GRADE) --make tl2.clean

.PHONY: clean all repl
.IGNORE: clean

