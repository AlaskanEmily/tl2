%=============================================================================%
% Copyright (c) 2023-2024 AlaskanEmily, Transnat Games.
% Licensed under BSD 4-clause (Trans Rights notice).
:- module tl2.
%=============================================================================%
% TurboLisp kernel
:- interface.
%=============================================================================%

:- use_module kv_list.
:- use_module io.
:- use_module maybe.
:- import_module list.
:- use_module rbtree.

:- include_module tl2.parser.
:- include_module tl2.rt.

%-----------------------------------------------------------------------------%

:- type subr == (pred(info, lst, s)).
:- mode subr == (pred(in, in, out) is det).
:- inst subr == (pred(in, in, out) is det).

%-----------------------------------------------------------------------------%

:- type subr_ext == (pred(info, lst, env, s, defs, defs, io.io, io.io)).
:- mode subr_ext == (pred(in, in, in, out, in, out, di, uo) is det).
:- inst subr_ext == (pred(in, in, in, out, in, out, di, uo) is det).

%----------------------------------------------------------------------------%

:- type prop_data --->
    apname(string) ;
    apval(s) ;
    expr(lst) ;
    fexpr(lst) ;
    subr(subr, lst) ;
    fsubr(subr, lst) ;
    subr_ext(subr_ext, lst) ;
    fsubr_ext(subr_ext, lst).

%-----------------------------------------------------------------------------%

:- type props == list.list(prop_data).

%-----------------------------------------------------------------------------%

:- type lst == list.list(s).

%-----------------------------------------------------------------------------%

:- type env == kv_list.kv_list(string, s).

%-----------------------------------------------------------------------------%
% Implementation note:
% This is used to maintain the def table, which was ill-defined in Lisp 1.5
% and operated more like a goto table.
% Using this also allows us to not include the LABEL construct.
:- type defs == rbtree.rbtree(string, props).

%-----------------------------------------------------------------------------%

:- type filename == string.

%-----------------------------------------------------------------------------%

:- type lineno == int.

%-----------------------------------------------------------------------------%

:- type line_data ---> line_data(filename::filename, lineno::lineno).

%-----------------------------------------------------------------------------%

:- type info == maybe.maybe(line_data).

%-----------------------------------------------------------------------------%

:- func line_prefix(info::in) = (string::uo) is det.

%-----------------------------------------------------------------------------%

:- type s --->
    list(info, lst) ;
    atom(info, string) ;
    string(info, string) ;
    int(info, int).

%-----------------------------------------------------------------------------%

:- func info(s) = info.

%-----------------------------------------------------------------------------%

:- func update_val(props, s) = props.
:- pred update_val(s::in, props::in, props::out) is det.

%-----------------------------------------------------------------------------%

:- inst yes for maybe.maybe/1 == bound(maybe.yes(ground)).
:- inst no for maybe.maybe/1 == bound(maybe.no).

:- inst empty for list.list/1 == bound([]).
:- inst one for list.list/1 == bound([ground|empty]).
:- inst one_or_more for list.list/1 == bound([ground|ground]).
:- inst pair for list.list/1 == bound([ground|one]).

:- inst int for s/0 == bound(int(ground, ground)).
:- inst atom for s/0 == bound(atom(ground, ground)).
:- inst list for s/0 == bound(list(ground, ground)).
:- inst list_empty for s/0 == bound(list(ground, empty)).
:- inst list_pair for s/0 == bound(list(ground, pair)).
:- inst list_one_or_more for s/0 == bound(list(ground, one_or_more)).

%-----------------------------------------------------------------------------%
% nil = list([]).
:- func nil = (s::out(list_empty)) is det.

%-----------------------------------------------------------------------------%
% t = atom("t").
:- func t = (s::out(atom)) is det.

%-----------------------------------------------------------------------------%

:- func find(string, env, defs) = props.

%-----------------------------------------------------------------------------%
% This doesn't require IO in itself, but since it can invoke Mercury binds that
% are allowed to do IO.
:- pred apply(s, lst, env, s, defs, defs, io.io, io.io).
:- mode apply(in, in, in, out, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% This doesn't require IO in itself, but since it can invoke Mercury binds that
% are allowed to do IO.
:- pred eval(s, env, s, defs, defs, io.io, io.io).
:- mode eval(in, in, out, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% For use with list.foldl3 to eval a tail.
:- pred evlis(env, s, s, s, defs, defs, io.io, io.io).
:- mode evlis(in, in, in, out, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% For use with list.map_foldl2 to eval args.
:- pred evlis(env, s, s, defs, defs, io.io, io.io).
:- mode evlis(in, in, out, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred eqv(s::in, s::in) is semidet.

%-----------------------------------------------------------------------------%

:- pred eqv_list(lst::in, lst::in) is semidet.

%-----------------------------------------------------------------------------%

:- pred sassoc(s::in, lst::in, s::out(list_one_or_more)) is semidet.

%-----------------------------------------------------------------------------%

:- pred replace_assoc(pred(tl2.lst, tl2.lst), tl2.s, tl2.lst, tl2.lst).
:- mode replace_assoc(pred(in, out) is det, in, in, out) is det.
:- mode replace_assoc(pred(in, out) is semidet, in, in, out) is semidet.

%-----------------------------------------------------------------------------%

:- pred spread_assoc(s::out, lst::in, lst::out) is semidet.

%-----------------------------------------------------------------------------%

:- pred special_form(s::in) is semidet.

%-----------------------------------------------------------------------------%
% TODO: This isn't really very good.
:- func to_string(s::in) = (string::uo) is det.

%-----------------------------------------------------------------------------%

:- pred cmp_map(builtin.comparison_result::uo, tl2.s::in, tl2.s::in) is det.
% For use in list.sort*
:- pred cmp_map2(tl2.s::in, tl2.s::in, builtin.comparison_result::uo) is det.

%-----------------------------------------------------------------------------%
% TODO: This is here for its use in project Bowie, but it should perhaps not be
% placed in the interface.

:- type bloom == int.
:- func init_bloom = bloom.
:- func bloom(s) = bloom.
:- func bloom(s, bloom) = bloom.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.
:- import_module int.
:- use_module string.

%-----------------------------------------------------------------------------%

info(atom(Info, _)) = Info.
info(string(Info, _)) = Info.
info(int(Info, _)) = Info.
info(list(Info, _)) = Info.
:- pragma inline(info/1).

%-----------------------------------------------------------------------------%

update_val(!.Props, S) = !:Props :- update_val(S, !.Props, [], !:Props).

update_val(S, Props, update_val(Props, S)).

:- pred update_val(s::in, props::in, props::in, props::out) is det.
update_val(S, [], Props, [apval(S)|list.reverse(Props)]).
update_val(S, [Head|Tail], !Props) :-
    ( if
        Head = apval(_)
    then
        list.cons(apval(S), !Props),
        list.reverse_prepend(!.Props, Tail, !:Props)
    else
        list.cons(Head, !Props),
        update_val(S, Tail, !Props)
    ).

%-----------------------------------------------------------------------------%

nil = list(maybe.no, []).
:- pragma inline(nil/0).

%-----------------------------------------------------------------------------%

t = atom(maybe.no, "t").
:- pragma inline(t/0).

%-----------------------------------------------------------------------------%

find(Name, Env, Defs) = !:P :-
    ( rbtree.search(Defs, Name, S1) -> !:P = S1 ; !:P = [] ),
    ( kv_list.search(Env, Name, S2) -> list.cons(apval(S2), !P) ; true ).
:- pragma inline(find/3).

%-----------------------------------------------------------------------------%

:- func line_prefix(string::in, int::in) = (string::uo) is det.
line_prefix(Filename, Lineno) = string.append(A, B) :-
    string.append("! ", Filename, A) &
    string.first_char(B, (':'), string.from_int(Lineno)).
:- pragma foreign_proc("C",
    line_prefix(Filename::in, Lineno::in) = (Out::uo),
    [promise_pure, thread_safe, may_duplicate, will_not_call_mercury,
    will_not_throw_exception],
    "
#define TL2_LINE_PREFIX_MAX (sizeof(MR_Integer) == 4 ? 11 : 22)
#define TL2_LINE_PREFIX_FMT "":%"" MR_INTEGER_LENGTH_MODIFIER ""d""
    const MR_Integer len = strlen(Filename);
    const MR_Integer suffix_len = (Lineno < 1000) ? 4 : TL2_LINE_PREFIX_MAX;
    MR_allocate_aligned_string_msg(Out, len + suffix_len, MR_ALLOC_ID);
    memcpy(Out, Filename, len);
    snprintf(Out, suffix_len + 1, TL2_LINE_PREFIX_FMT, Lineno);
#undef TL2_LINE_PREFIX_FMT
#undef TL2_LINE_PREFIX_MAX
    ").
:- pragma inline(line_prefix/2).

%-----------------------------------------------------------------------------%

line_prefix(maybe.no) = "!".
line_prefix(maybe.yes(P)) = line_prefix(P ^ filename, P ^ lineno).
% Do not inline, this is the cold path.
:- pragma no_inline(line_prefix/1).

%-----------------------------------------------------------------------------%

:- func atom_to_string(s) = string.
:- mode atom_to_string(in) = (out) is det.
:- mode atom_to_string(mdi) = (muo) is det.
:- mode atom_to_string(di) = (uo) is det.
atom_to_string(S) = Out :-
    ( if
        S = atom(_, Semi)
    then
        Out = Semi
    else
        string.append(line_prefix(info(S)), " Value is not an atom", Error),
        exception.throw(exception.software_error(Error))
    ).
:- pragma inline(atom_to_string/1).

%-----------------------------------------------------------------------------%

:- pred pair_env(s, s, env, env).
:- mode pair_env(in, in, in, out) is det.
:- mode pair_env(mdi, mdi, mdi, muo) is det.
:- mode pair_env(di, di, di, uo) is det.
pair_env(Key, Val, Env, kv_list.kv_cons(atom_to_string(Key), Val, Env)).
:- pragma inline(pair_env/4).

%-----------------------------------------------------------------------------%

:- pred make_env_pars(lst, lst, env, env).
:- mode make_env_pars(in, in, in, out) is semidet.
:- mode make_env_pars(mdi, mdi, mdi, muo) is semidet.
:- mode make_env_pars(di, di, di, uo) is semidet.

make_env_pars([], [], !Env).
make_env_pars([Car1|Cdr1], [Car2|Cdr2], !Env) :-
    pair_env(Car1, Car2, !Env),
    make_env_pars(Cdr1, Cdr2, !Env).
:- pragma inline(make_env_pars/4).

%-----------------------------------------------------------------------------%

evlis(Env, Form, _, Out, !Defs, !IO) :-
    tl2.eval(Form, Env, Out, !Defs, !IO).
:- pragma inline(evlis/8).

%-----------------------------------------------------------------------------%

evlis(Env, Form, Out, !Defs, !IO) :-
    tl2.eval(Form, Env, Out, !Defs, !IO).
:- pragma inline(evlis/7).

%-----------------------------------------------------------------------------%
% Default case of apply.
% This is just for convenience, since our disjunction structure means this
% will happen in a few different places.
:- pred apply_eval(s, lst, env, s, defs, defs, io.io, io.io).
:- mode apply_eval(in, in, in, out, in, out, di, uo) is det.
apply_eval(Fn, Args, Env, Out, !Defs, !IO) :-
    tl2.eval(Fn, Env, Form, !Defs, !IO),
    % This is not quite correct for Lisp 1.5, it would unconditionally apply.
    % However, this leads to an infinite loop for applying unbound value atoms,
    % which for us include all non-list atoms.
    ( if
        Form = list(_, _)
    then
        tl2.apply(Form, Args, Env, Out, !Defs, !IO)
    else
        Out = Form
    ).
:- pragma inline(apply_eval/8).

%-----------------------------------------------------------------------------%
% This converts from a Lisp list into our env format.
:- pred unwrap_env(lst::in, env::in, env::out) is det.
unwrap_env([], !Env).
unwrap_env([Car|Cdr], !Env) :-
    ( if
        Car = list(_, [Key|[Val|_]])
    then
        pair_env(Key, Val, !Env),
        unwrap_env(Cdr, !Env)
    else
        string.append(line_prefix(info(Car)), " Invalid binds", Error),
        exception.throw(exception.software_error(Error))
    ).

%-----------------------------------------------------------------------------%
% This evaluates binds, such as in function args or let binds.
:- pred unwrap_env(lst, env, env, defs, defs, io.io, io.io).
:- mode unwrap_env(in, in, out, in, out, di, uo) is det.
unwrap_env([], !Env, !Defs, !IO).
unwrap_env([Car|Cdr], !Env, !Defs, !IO) :-
    ( if
        Car = list(_, [Key|[Val|_]])
    then
        tl2.eval(Val, !.Env, NewVal, !Defs, !IO),
        pair_env(Key, NewVal, !Env),
        unwrap_env(Cdr, !Env, !Defs, !IO)
    else
        string.append(line_prefix(info(Car)), " Invalid binds", Error),
        exception.throw(exception.software_error(Error))
    ).

%-----------------------------------------------------------------------------%

apply(Fn, Args, Env, Out, !Defs, !IO) :-
    ( if
        Fn = list(_, [])
    then
        Out = nil
    else if
        Fn = atom(Info, A)
    then
        ( if
            rbtree.search(!.Defs, A, P),
            ( if
                list.find_first_match(pred(expr(_)::in) is semidet, P, Expr)
            then
                Expr = expr(Body),
                Action = tl2.apply(list(maybe.no, Body))
            else
                false
            )
        then
            Action(Args, Env, Out, !Defs, !IO)
        else
            % Check if this atom is bound by the environment.
            ( if
                kv_list.search(Env, A, Val)
            then
                % Re-apply using the found value.
                tl2.apply(Val, Args, Env, Out, !Defs, !IO)
            else
                string.append(" X Unbound variable: ", A, B),
                string.append(line_prefix(Info), B, Error),
                exception.throw(exception.software_error(Error))
            )
        )
    else if
        Fn = list(Info, [atom(_, Car)|Cdr])
    then
        ( if
            Car = "lambda"
        then
            % Lambda in Lisp 1.5 terms is just the code, no data bindings.
            % The data bindings are created by the 'function' special form,
            % which is transformed into the 'funargs' form by eval.
            ( if
                Cdr = [list(ArgsInfo, L)|Cddr]
            then
                (
                    Cddr = [],
                    Out = nil
                ;
                    Cddr = [_|_],
                    % Generate the argument bindings.
                    % TODO: This should throw a better exception if the arg
                    % counts are mismatched.
                    % list.foldl_corresponding(pair_env, L, Args, Env, NewEnv),
                    ( if
                        make_env_pars(L, Args, Env, NewEnv)
                    then
                        % Implementation note:
                        % Lisp 1.5 would have only eval'ed the caddr. We are
                        % folding over the entire cddr.
                        % This is more similar to Clojure, where the tail of a
                        % lambda is wrapped in an implicit 'do'.
                        list.foldl3(evlis(NewEnv), Cddr, nil, Out, !Defs, !IO)
                    else
                        Error = string.format(
"%s Invalid number of args for lambda defined at %s. Expected %i, actual %i", [
                            string.s(line_prefix(ArgsInfo))|[
                            string.s(line_prefix(Info))|[
                            string.i(list.length(Args))|[
                            string.i(list.length(L))|[]]]]]),
                        exception.throw(exception.software_error(Error))
                    )
                )
            else
                string.append(line_prefix(Info), " Invalid lambda", Error),
                exception.throw(exception.software_error(Error))
            )
        else if
            % This is poorly explained in the Lisp 1.5 paper. funarg is used
            % to capture state for a closure (called a "function" in Lisp 1.5)
            Car = "funargs"
        then
            ( if
                Cdr = [list(_, NewEnvList)|Cddr]
            then
                (
                    Cddr = [],
                    Out = nil
                ;
                    Cddr = [Caddr|_],
                    % Implementation note:
                    % Lisp 1.5 would have only eval'ed the caddr. We are
                    % folding over the entire cddr.
                    % This is more similar to Clojure, where the tail of a
                    % lambda is wrapped in an implicit 'do'.
                    unwrap_env(NewEnvList, kv_list.kv_nil, NewEnv),
                    tl2.apply(Caddr, Args, NewEnv, Out, !Defs, !IO)
                    % list.foldl3(evlis(NewEnv), Cddr, nil, Out, !Defs, !IO)
                )
            else
                string.append(line_prefix(Info), " Invalid funargs", Error),
                exception.throw(exception.software_error(Error))
            )
        else
            % Implementation note:
            % We use the def table, rather than implementing LABEL.
            % If we were implementing a strict Lisp 1.5 interpreter, this is
            % where LABEL would go.
            apply_eval(Fn, Args, Env, Out, !Defs, !IO)
        )
    else
        apply_eval(Fn, Args, Env, Out, !Defs, !IO)
    ).

%-----------------------------------------------------------------------------%

:- pred evcon(lst, env, s, s, defs, defs, io.io, io.io).
:- mode evcon(in, in, in, out, in, out, di, uo) is det.
evcon([], _Env, !Val, !Defs, !IO).
evcon([Car|Cdr], Env, !Val, !Defs, !IO) :-
    ( if
        Car = list(_, [Caar|Cadr])
    then
        tl2.eval(Caar, Env, CondVal, !Defs, !IO),
        ( if
            CondVal = atom(_, "t")
        then
            list.foldl3(evlis(Env), Cadr, !Val, !Defs, !IO)
        else
            evcon(Cdr, Env, !Val, !Defs, !IO)
        )
    else
        string.append(line_prefix(info(Car)), " Invalid cond case", Error),
        exception.throw(exception.software_error(Error))
    ).

%-----------------------------------------------------------------------------%
% cast_subr/cast_subr_ext/eval_from all exist to cast the inst-ness back into
% subr/subr_ext after it's been taken to ground.
%
% This is working around a limitation in the Mercury compiler regarding storing
% preds/funcs, where they may lose their pred insts and become ground.
%-----------------------------------------------------------------------------%

:- func cast_subr(subr::in) = (subr::out(subr)) is det.
:- pragma foreign_proc("C",
    cast_subr(I::in) = (O::out(subr)),
    [promise_pure, thread_safe, does_not_affect_liveness, may_duplicate,
    will_not_call_mercury, will_not_throw_exception],
    "O=I;").
:- pragma foreign_proc("Java",
    cast_subr(I::in) = (O::out(subr)),
    [promise_pure, thread_safe, may_duplicate, will_not_call_mercury],
    "O=I;").
:- pragma foreign_proc("C#",
    cast_subr(I::in) = (O::out(subr)),
    [promise_pure, thread_safe, may_duplicate, will_not_call_mercury],
    "O=I;").
:- pragma inline(cast_subr/1).

%-----------------------------------------------------------------------------%

:- func cast_subr_ext(subr_ext::in) = (subr_ext::out(subr_ext)) is det.
:- pragma foreign_proc("C",
    cast_subr_ext(I::in) = (O::out(subr_ext)),
    [promise_pure, thread_safe, does_not_affect_liveness, may_duplicate,
    will_not_call_mercury, will_not_throw_exception],
    "O=I;").
:- pragma foreign_proc("Java",
    cast_subr_ext(I::in) = (O::out(subr_ext)),
    [promise_pure, thread_safe, may_duplicate, will_not_call_mercury],
    "O=I;").
:- pragma foreign_proc("C#",
    cast_subr_ext(I::in) = (O::out(subr_ext)),
    [promise_pure, thread_safe, may_duplicate, will_not_call_mercury],
    "O=I;").
:- pragma inline(cast_subr_ext/1).

%-----------------------------------------------------------------------------%

:- pred eval_from(props, s, lst, env, s, defs, defs, io.io, io.io).
:- mode eval_from(in, in(atom), in, in, out, in, out, di, uo) is det.

eval_from([apname(_)|Tail], Atom, Args, Env, Out, !Defs, !IO) :-
    eval_from(Tail, Atom, Args, Env, Out, !Defs, !IO).

eval_from([apval(Val)|_], _Atom, Args, Env, Out, !Defs, !IO) :-
    tl2.eval(list(maybe.no, [Val|Args]), Env, Out, !Defs, !IO).

eval_from([expr(Body)|_], _Atom, !.Args, Env, Out, !Defs, !IO) :-
    list.map_foldl2(evlis(Env), !Args, !Defs, !IO),
    tl2.apply(list(maybe.no, Body), !.Args, Env, Out, !Defs, !IO).

eval_from([fexpr(Body)|_], _Atom, Args, Env, Out, !Defs, !IO) :-
    tl2.apply(list(maybe.no, Body), Args, Env, Out, !Defs, !IO).

eval_from([subr(Subr, _)|_], atom(Info, _), !.Args, Env, Out, !Defs, !IO) :-
    list.map_foldl2(evlis(Env), !Args, !Defs, !IO),
    call(cast_subr(Subr), Info, !.Args, Out).

eval_from([fsubr(Subr, _)|_], atom(Info, _), Args, _Env, Out, !Defs, !IO) :-
    call(cast_subr(Subr), Info, Args, Out).

eval_from([subr_ext(Subr, _)|_], atom(Info, _), !.Args, Env, Out, !Defs, !IO) :-
    list.map_foldl2(evlis(Env), !Args, !Defs, !IO),
    call(cast_subr_ext(Subr), Info, !.Args, Env, Out, !Defs, !IO).

eval_from([fsubr_ext(Subr, _)|_], atom(Info, _), Args, Env, Out, !Defs, !IO) :-
    call(cast_subr_ext(Subr), Info, Args, Env, Out, !Defs, !IO).

eval_from([], atom(Info, A), Args, Env, Out, !Defs, !IO) :-
    ( if
        kv_list.search(Env, A, Val)
    then
        tl2.eval(list(maybe.no, [Val|Args]), Env, Out, !Defs, !IO)
    else
        string.append(" Y Unbound variable: ", A, B),
        string.append(line_prefix(Info), B, Error),
        exception.throw(exception.software_error(Error))
    ).

%-----------------------------------------------------------------------------%

eval(Form, Env, Out, !Defs, !IO) :-
    (
        ( Form = list(_, []) ; Form = string(_, _) ; Form = int(_, _) ),
        Out = Form
    ;
        Form = list(_, [Car|Cdr]),
        ( if
            % Clojure extension, an explicit comment handler.
            Car = atom(_, "comment")
        then
            Out = nil
        else if
            Car = atom(_, "quote")
        then
            (
                Cdr = [],
                Out = nil
            ;
                Cdr = [Out|_]
            )
        else if
            % This could be implemented as a fsubr or fexpr, but for efficiency
            % it's done here as a builtin since it's just a one liner.
            Car = atom(Info, "list")
        then
            list.map_foldl2(evlis(Env), Cdr, List, !Defs, !IO),
            Out = list(Info, List)
        else if
            % CL extension, this is mostly identical to Scheme define.
            % TODO: Should this be in eval?
            Car = atom(Info, "def")
        then
            ( if
                Cdr = [atom(_, Name)|[Value|[]]]
            then
                tl2.eval(Value, Env, APVal, !Defs, !IO),
                rbtree.set(Name, [apval(APVal)|[]], !Defs),
                Out = nil
            else
                string.append(line_prefix(Info), " Invalid def", Error),
                exception.throw(exception.software_error(Error))
            )
        else if
            % This is a CL extension. We actually apply this implicitly to most
            % special forms, but this is the explicit version.
            Car = atom(_, "do")
        then
            list.foldl3(evlis(Env), Cdr, nil, Out, !Defs, !IO)
        else if
            % This is a Scheme-based extension, where specific bindings can be
            % provided.
            Car = atom(Info, "let")
        then
            ( if
                Cdr = [list(_, NewEnvList)|Cddr]
            then
                (
                    Cddr = [],
                    Out = nil
                ;
                    Cddr = [_|_],
                    % TODO: Actually eval these?
                    unwrap_env(NewEnvList, Env, NewEnv, !Defs, !IO),
                    list.foldl3(evlis(NewEnv), Cddr, nil, Out, !Defs, !IO)
                )
            else
                string.append(line_prefix(Info), " Invalid let", Error),
                exception.throw(exception.software_error(Error))
            )
        else if
            Car = atom(Info, "fn")
        then
            % Special form, this combines function and lambda into one.
            % This is modeled after Common Lisp.
            (
                Cdr = [],
                Body = []
            ;
                Cdr = [_|Tail],
                Lambda = list(Info, [atom(Info, "lambda")|Cdr]),
                make_funargs(Tail, Env, NewEnv),
                Body = [atom(Info, "funargs")|[list(Info, NewEnv)|[Lambda|[]]]]
            ),
            Out = list(Info, Body)
        else if
            Car = atom(Info, "function")
        then
            % Implementation note:
            % This uses the entire Cdr, rather than the Cadr. This is because,
            % unlike Lisp 1.5, all function and lambda expressions have an
            % implicit 'do' in this implementation.
            % This is more similar to how Clojure works.
            make_funargs(Cdr, Env, NewEnv),
            Out = list(Info, [atom(Info, "funargs")|[list(Info, NewEnv)|Cdr]])
        else if
            Car = atom(_, "cond")
        then
            evcon(Cdr, Env, nil, Out, !Defs, !IO)
        else if
            Car = atom(Info, A),
            % If there is a def entry for this atom, evaluate that.
            rbtree.search(!.Defs, A, P)
        then
            % See the note above about why eval_from is so janky.
            eval_from(P, atom(Info, A), Cdr, Env, Out, !Defs, !IO)
        else
            % Implementation note:
            % We do not implement PROG, but this is where it would go.
            
            % TODO: This is a huge hack! We should find out why this isn't
            % an issue in other implementations!
            (
                Cdr = [],
                Args = []
            ;
                Cdr = [Cadr|Cddr],
                ( if
                    Car = atom(_, Tag),
                    ( Tag = "lambda" ; Tag = "funargs" )
                then
                    list.map_foldl2(evlis(Env), Cddr, Args1, !Defs, !IO),
                    Args = [Cadr|Args1]
                else
                    list.map_foldl2(evlis(Env), Cdr, Args, !Defs, !IO)
                )
            ),
            tl2.apply(Car, Args, Env, Out, !Defs, !IO)
        )
    ;
        Form = atom(Info, A),
        ( if
            ( if
                % Check the def table.
                rbtree.search(!.Defs, A, P),
                list.find_first_match(pred(apval(_)::in) is semidet, P, Prop)
            then
                % This is technically semidet, but we know because of the
                % above find-first-match that it must succeed.
                Prop = apval(Val)
            else
                % Check if this atom is bound by the environment.
                kv_list.search(Env, A, Val)
            )
        then
            Out = Val
        else
            string.append(" Z Unbound variable: ", A, B),
            string.append(line_prefix(Info), B, Error),
            exception.throw(exception.software_error(Error))
        )
    ).

%-----------------------------------------------------------------------------%

eqv(list(_, List1), list(_, List2)) :-
    eqv_list(List1, List2).
eqv(atom(_, A), atom(_, A)).
eqv(string(_, S), string(_, S)).
eqv(int(_, I),  int(_, I)).

%-----------------------------------------------------------------------------%

eqv_list([], []).
eqv_list([Car1|Cdr1], [Car2|Cdr2]) :-
    eqv(Car1, Car2),
    eqv_list(Cdr1, Cdr2).

%-----------------------------------------------------------------------------%

sassoc(K, [H|T], Out) :-
    ( if
        H = list(N, [A|L]),
        eqv(A, K)
    then
        Out = list(N, [A|L])
    else
        sassoc(K, T, Out)
    ).

%-----------------------------------------------------------------------------%

:- pred replace_assoc(pred(tl2.lst, tl2.lst), tl2.s, tl2.lst, tl2.lst, tl2.lst).
:- mode replace_assoc(pred(in, out) is det, in, in, in, out) is det.
:- mode replace_assoc(pred(in, out) is semidet, in, in, in, out) is semidet.
replace_assoc(_Pred, _Key, [], !Lst).
replace_assoc(Pred, K, [H|T], !Lst) :-
    ( if
        H = list(_, List),
        List = [A|_],
        eqv(A, K)
    then
        Pred(List, New),
        list.cons(tl2.list(maybe.no, New), !Lst),
        list.reverse_prepend(!.Lst, T, !:Lst)
    else
        list.cons(H, !Lst),
        replace_assoc(Pred, K, T, !Lst)
    ).

replace_assoc(Pred, K, !Lst) :-
    replace_assoc(Pred, K, [], !Lst).

%-----------------------------------------------------------------------------%

spread_assoc(H, [list(_, [_|[H|[]]])|T], T).

%-----------------------------------------------------------------------------%

special_form(atom(_, "nil")).
special_form(atom(_, "t")).
special_form(atom(_, "cond")).
special_form(atom(_, "let")).
special_form(atom(_, "def")).
special_form(atom(_, "lambda")).
special_form(atom(_, "function")).
special_form(atom(_, "funargs")).
special_form(atom(_, "list")).
special_form(list(_, [])).
special_form(list(_, [atom(_, "quote")])).

%-----------------------------------------------------------------------------%

to_string(list(_, [])) = "nil".
to_string(list(_, [_|_] @ !.List)) = string.append(Out, ")") :-
    list.foldl(
        (pred(X::in, !.XI::di, !:XI::uo) is det :-
            ( if
                !.XI = ""
            then
                string.first_char(!:XI, ('('), tl2.to_string(X))
            else
                string.first_char(XMid, (' '), tl2.to_string(X)),
                string.append(!.XI, XMid, !:XI)
            )
        ),
        !.List,
        "", Out).
to_string(atom(_, !.A)) = !:A :-
    ( !.A = "nil" -> !:A = "'nil" ; builtin.copy(!A) ).

to_string(string(_, S)) = string.append("""", string.append(S, """")).
to_string(int(_, I)) = string.from_int(I).
:- pragma inline(to_string/1).

%-----------------------------------------------------------------------------%
% It's kind of ugly, but easy to just list out the comparisons.
% Ordinals:
% list = 0,
% atom = 1,
% string = 2,
% int = 3

cmp_map((<), tl2.list(_, _), tl2.atom(_, _)).
cmp_map((<), tl2.list(_, _), tl2.string(_, _)).
cmp_map((<), tl2.list(_, _), tl2.int(_, _)).
cmp_map((<), tl2.list(_, []), tl2.list(_, [_|_])).
cmp_map((>), tl2.list(_, [_|_]), tl2.list(_, [])).
cmp_map((=), tl2.list(_, []), tl2.list(_, [])).
cmp_map(Cmp, tl2.list(_, [A|_]), tl2.list(_, [B|_])) :- cmp_map(Cmp, A, B).

cmp_map((>), tl2.atom(_, _), tl2.list(_, _)).
cmp_map(Cmp, tl2.atom(_, A), tl2.atom(_, B)) :- builtin.compare(Cmp, A, B).
cmp_map((<), tl2.atom(_, _), tl2.string(_, _)).
cmp_map((<), tl2.atom(_, _), tl2.int(_, _)).

cmp_map((>), tl2.string(_, _), tl2.list(_, _)).
cmp_map((>), tl2.string(_, _), tl2.atom(_, _)).
cmp_map(Cmp, tl2.string(_, A), tl2.string(_, B)) :- builtin.compare(Cmp, A, B).
cmp_map((<), tl2.string(_, _), tl2.int(_, _)).

cmp_map((>), tl2.int(_, _), tl2.list(_, _)).
cmp_map((>), tl2.int(_, _), tl2.atom(_, _)).
cmp_map((>), tl2.int(_, _), tl2.string(_, _)).
cmp_map(Cmp, tl2.int(_, A), tl2.int(_, B)) :- builtin.compare(Cmp, A, B).
:- pragma inline(cmp_map/3).

cmp_map2(A, B, Cmp) :- cmp_map(Cmp, A, B).
:- pragma inline(cmp_map2/3).

%-----------------------------------------------------------------------------%

:- func bloom2(s, bloom) = bloom.
bloom2(list(_, List), Bloom) = list.foldl(bloom, List, Bloom).
bloom2(atom(_, A), Bloom) = Bloom \/ string.hash(A).
bloom2(string(_, _), Bloom) = Bloom.
bloom2(int(_, _), Bloom) = Bloom.

%-----------------------------------------------------------------------------%

init_bloom = 0.
bloom(S, In) = Out :-
    ( special_form(S) -> In = Out ; Out = bloom2(S, In) ).
bloom(S) = bloom(S, init_bloom).

%-----------------------------------------------------------------------------%

:- pred use_direct_env(int::in, env::in) is semidet.
use_direct_env(_, kv_list.kv_nil).
use_direct_env(I, kv_list.kv_cons(_, _, T)) :-
    I < 8,
    use_direct_env(I+1, T).

%-----------------------------------------------------------------------------%

:- pred make_funargs2(int::in, env::in, env::out, lst::in, lst::out) is det.
make_funargs2(_, kv_list.kv_nil, kv_list.kv_nil, !Env).
make_funargs2(I, kv_list.kv_cons(K, V, T), Out, !Env) :-
    list.cons(list(maybe.no, [atom(maybe.no, K)|[V|[]]]), !Env),
    ( if
        I < 8
    then
        make_funargs2(I + 1, T, Out, !Env)
    else
        T = Out
    ).

%-----------------------------------------------------------------------------%

:- pred make_funargs(lst::in, env::in, lst::out) is det.
make_funargs(Body, !.Env, !:NewEnv) :-
    % This will fully convert any small env list greedily, and will not throw
    % away that work when we switch to a bloom filter.
    !:NewEnv = [],
    make_funargs2(0, !Env, !NewEnv),
    (
        !.Env = kv_list.kv_nil
    ;
        !.Env = kv_list.kv_cons(_, _, _),
        % This uses a bloom map to ensure that this function executes in
        % linear time.
        list.foldl(bloom, Body, init_bloom) = Bloom,
        kv_list.foldl(
            (pred(XA::in, XV::in, !.X::in, !:X::out) is det :-
                XHash = string.hash(XA),
                ( if
                    XHash /\ Bloom = XHash
                then
                    list.cons(list(maybe.no, [atom(maybe.no, XA)|[XV|[]]]), !X)
                else
                    true
                )
            ),
            !.Env,
            !NewEnv)
    ).

